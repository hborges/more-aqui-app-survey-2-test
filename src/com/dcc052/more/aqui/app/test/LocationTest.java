package com.dcc052.more.aqui.app.test;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.test.ActivityInstrumentationTestCase2;

import com.dcc052.more.aqui.app.InsertActivity;

public class LocationTest extends ActivityInstrumentationTestCase2<InsertActivity> {
	
	private static final String TEST_PROVIDER_NAME = "LocationTest.testLocationCriteria()";

	private InsertActivity mActivity;

	private LocationManager locationManager;

	private static final String MESSAGE_NULL_PROVIDER = "Provedor de localização inválido! O nome não deve ser nulo.";

	private static final String MESSAGE_ENABLED_PROVIDER = "Use somente provedores de localização habilitados!";

	private static final String MESSAGE_CRITERIA_COST = "A utilização do provedor não pode acarretar em custos monetários!";
	private static final String MESSAGE_CRITERIA_ACCURACY = "O provedor deve preferencialmente ter boa acurácia!";
	private static final String MESSAGE_CRITERIA_POWER = "O provedor deve preferencialmente consumir baixa energia!";

	private static final String MESSAGE_CRITERIA = "Os critérios utilizados não estão corretos!";

	private static final String MESSAGE_LOCATION_LATITUDE = "A latitude retorna não está correta!";

	private static final String MESSAGE_LOCATION_LONGITUDE = "A longitude retorna não está correta!";

	public LocationTest() {
		super(InsertActivity.class);
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.mActivity = super.getActivity();
		this.locationManager = (LocationManager) this.mActivity.getSystemService(Context.LOCATION_SERVICE);
		
		this.removeTestLocationProvider();
		this.addTestLocationProvider();
	}
	
	public void testCriterios() {
		// Execute the implemented task
		this.mActivity.getLocation();
				
		final Criteria criteria = this.mActivity.getCriteria();
		assertFalse(MESSAGE_CRITERIA_COST, criteria.isCostAllowed());
		assertEquals(MESSAGE_CRITERIA_ACCURACY, criteria.getAccuracy(), Criteria.ACCURACY_FINE);
		assertEquals(MESSAGE_CRITERIA_POWER, criteria.getPowerRequirement(), Criteria.POWER_LOW);
	}
	
	public void testLocalizacaoCorreta() {
		// Execute the implemented task
		double[] coordinates = this.mActivity.getLocation();
		
		LocationProvider taskProvider = locationManager.getProvider(this.mActivity.getBestProviderName());
		assertTrue(MESSAGE_CRITERIA, taskProvider.getName().equals(TEST_PROVIDER_NAME));
				
		Location location = locationManager.getLastKnownLocation(TEST_PROVIDER_NAME);
		assertEquals(MESSAGE_LOCATION_LATITUDE, coordinates[0], location == null ? 0 : location.getLatitude());
		assertEquals(MESSAGE_LOCATION_LONGITUDE, coordinates[1], location == null ? 0 : location.getLongitude());
	}
	
	public void testProvedorCorreto () {
		// Set test provider not enabled 
		locationManager.setTestProviderEnabled(TEST_PROVIDER_NAME, false);
		
		// Execute the implemented task
		this.mActivity.getLocation();
		
		assertNotNull(MESSAGE_NULL_PROVIDER, this.mActivity.getBestProviderName());
		assertTrue(MESSAGE_ENABLED_PROVIDER, locationManager.isProviderEnabled(this.mActivity.getBestProviderName()));
		
		// Set test provider enabled 
		locationManager.setTestProviderEnabled(TEST_PROVIDER_NAME, true);
		
		// Execute the implemented task
		this.mActivity.getLocation();
				
		assertNotNull(MESSAGE_NULL_PROVIDER, this.mActivity.getBestProviderName());
		assertTrue(MESSAGE_ENABLED_PROVIDER, locationManager.isProviderEnabled(this.mActivity.getBestProviderName()));
	}

	@Override
	protected void tearDown() throws Exception {
		this.removeTestLocationProvider();
		super.tearDown();
	}
	
	private void addTestLocationProvider() {
		locationManager.addTestProvider(TEST_PROVIDER_NAME, false, true, true, false, false, false, false, Criteria.POWER_LOW, Criteria.ACCURACY_FINE);

		Location locationTest = new Location(TEST_PROVIDER_NAME);
		locationTest.setLatitude(123456789);
		locationTest.setLongitude(987654321);
		
		locationManager.setTestProviderLocation(TEST_PROVIDER_NAME, locationTest);
		locationManager.setTestProviderEnabled(TEST_PROVIDER_NAME, true);
	}
	
	private void removeTestLocationProvider() {
		LocationProvider provider = locationManager.getProvider(TEST_PROVIDER_NAME);
		if (provider != null) {
			locationManager.removeTestProvider(provider.getName());
		}
	}
	
}

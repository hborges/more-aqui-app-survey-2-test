package com.dcc052.more.aqui.app.test;

import static com.dcc052.more.aqui.app.entity.DBConstants.TABLE_NAME;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteReadOnlyDatabaseException;
import android.provider.BaseColumns;
import android.test.ActivityInstrumentationTestCase2;
import android.widget.TextView;

import com.dcc052.more.aqui.app.InsertActivity;
import com.dcc052.more.aqui.app.R;
import com.dcc052.more.aqui.app.entity.DBConstants;
import com.dcc052.more.aqui.app.entity.Estate;
import com.dcc052.more.aqui.app.entity.EstatesData;

public class InsertTaskTest extends
		ActivityInstrumentationTestCase2<InsertActivity> {
	
	private static final String TYPE = "Apartment";
	private static final String SIZE = "Small";
	private static final int PHONE = 99999999;
	private static final String UNDER_CONTRUCTION = Boolean.toString(true);
	private static final double LATITUDE = 0d;
	private static final double LONGITUDE = 0d;
	
	private static final String DEFAULT_MESSAGE = "Os dados não estão sendo persistidos com sucesso!";
	private static final String MESSAGE_CONTENT = "Os dados inseridos não conferem!";
	private static final String MESSAGE_CONNECTION = "A conexão com o banco de dados não foi devidamente tratada!";
	private static final String MESSAGE_CONNECTION_IN_TRANSACTION = "Uma transação está sendo aberta e não encerrada!";
	private static final String MESSAGE_NOT_CLOSED_CONNECTION = "A conexão com o bando de dados deve ser encerrada após o uso!";
	private static final String MESSAGE_READ_ONLY = "A conexão com o banco deve ser em modo de escrita para a persistencia de dados!";
	private static final String MESSAGE_EXCEPTION_NOT_HANDLED = "O código não está tratando problemas na persistencia dos dados!";
	private static final String MESSAGE_ACTION_RESULT = "As mensagens ao usuário não estão sendo apresentadas!";
	private static final String MESSAGE_PROBLEM_RESULT = "Falhas na inserção devem ser tratadas e relatadas devidamente aos usuários!";

	private InsertActivity mActivity;
	
	private EstatesData estatesData;
	
	public InsertTaskTest() {
		super(InsertActivity.class);
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.mActivity = super.getActivity();
		this.estatesData = new EstatesData(this.mActivity);
		this.estatesData.recreateDB();
	}
	
	public void testNotificacaoErro() {
		estatesData.destroyDB();
		
		try {
			this.mActivity.addEstate(new Estate(TYPE, SIZE, PHONE, UNDER_CONTRUCTION, LATITUDE, LONGITUDE));
		} catch (SQLiteException ex) {
			fail(MESSAGE_EXCEPTION_NOT_HANDLED);
		} finally {
			this.closeOpenedConnection();
		}
		
		TextView tv = (TextView) this.mActivity.getLayout().findViewById(R.id.text);
		assertEquals(MESSAGE_ACTION_RESULT, tv.getText(), InsertActivity.ERROR_MESSAGE);
	}
	
	public void testNotificacaoSucesso() {
		try {
			this.mActivity.addEstate(new Estate(TYPE, SIZE, PHONE, UNDER_CONTRUCTION, LATITUDE, LONGITUDE));
		} catch (SQLiteException ex) {
			fail(MESSAGE_EXCEPTION_NOT_HANDLED);
		} finally {
			this.closeOpenedConnection();
		}
		
		TextView tv = (TextView) this.mActivity.getLayout().findViewById(R.id.text);
		assertEquals(MESSAGE_ACTION_RESULT, tv.getText(), InsertActivity.SUCCESS_MESSAGE);
	}
	
	public void testTratamentoExcessoes() {
		estatesData.destroyDB();
		
		try {
			this.mActivity.addEstate(new Estate(TYPE, SIZE, PHONE, UNDER_CONTRUCTION, LATITUDE, LONGITUDE));
		} catch (SQLiteException ex) {
			fail(MESSAGE_EXCEPTION_NOT_HANDLED);
		} finally {
			this.closeOpenedConnection();
		}
	}
	
	public void testConexao() {
		// Adiciona um objeto na base com o código da tarefa
		try {
			this.mActivity.addEstate(new Estate(TYPE, SIZE, PHONE, UNDER_CONTRUCTION, LATITUDE, LONGITUDE));
		} catch (SQLiteReadOnlyDatabaseException ex) {
			this.closeOpenedConnection();
			fail(MESSAGE_READ_ONLY);
		}
		
		// Verifica se a conexão foi feita corretamente e obtem uma nova
		SQLiteDatabase db = this.mActivity.getDb();
		assertFalse(MESSAGE_READ_ONLY, db.isReadOnly());

		if (db.isOpen() && db.inTransaction()) {
			db.endTransaction();
			db.close();
			fail(MESSAGE_CONNECTION_IN_TRANSACTION);
		}
		
		if (db.isOpen()) {
			db.close();
			fail(MESSAGE_NOT_CLOSED_CONNECTION);
		}
		
	}

	public void testInsert() {
		// Obtem o numero de registros da base
		SQLiteDatabase  db = estatesData.getReadableDatabase();
		int nBefore = db.query(TABLE_NAME, new String[] { BaseColumns._ID }, null, null, null, null, null).getCount();
		db.releaseReference();
		
		// Adiciona um objeto na base com o código da tarefa
		try {
			this.mActivity.addEstate(new Estate(TYPE, SIZE, PHONE, UNDER_CONTRUCTION, LATITUDE, LONGITUDE));
		} catch (Exception ex) {
			this.closeOpenedConnection();
			fail(MESSAGE_PROBLEM_RESULT);
		}
		
		TextView tv = (TextView) this.mActivity.getLayout().findViewById(R.id.text);
		assertEquals(MESSAGE_CONTENT, tv.getText(), InsertActivity.SUCCESS_MESSAGE);
		
		assertFalse(DEFAULT_MESSAGE, this.closeOpenedConnection());

		// Verifica se o objeto foi adicionado corretamente, inclusive os valores
		db = estatesData.getReadableDatabase();
		
		Cursor cursor = null;
		try {
			
			cursor = db.query(TABLE_NAME, new String[] { BaseColumns._ID,
					DBConstants.PHONE, DBConstants.SIZE, DBConstants.STATUS,
					DBConstants.TYPE, DBConstants.LAT, DBConstants.LONG },
					null, null, null, null, null);
			
			assertEquals(DEFAULT_MESSAGE, cursor.getCount(), nBefore+1);
			assertTrue(DEFAULT_MESSAGE, cursor.moveToLast());
			
			assertEquals(MESSAGE_CONTENT, cursor.getInt(1), PHONE);
			assertEquals(MESSAGE_CONTENT, cursor.getString(2), SIZE);
			assertEquals(MESSAGE_CONTENT, cursor.getString(3), UNDER_CONTRUCTION);
			assertEquals(MESSAGE_CONTENT, cursor.getString(4), TYPE);
			assertEquals(MESSAGE_CONTENT, cursor.getDouble(5), LATITUDE);
			assertEquals(MESSAGE_CONTENT, cursor.getDouble(6), LONGITUDE);
			
		} catch (Exception e) {
			fail(DEFAULT_MESSAGE);
		} finally {
			db.close();
		}
		
	}

	@Override
	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	private boolean closeOpenedConnection(){
		boolean hasProblem = false;
		SQLiteDatabase db = this.mActivity.getDb();
		if (db != null) {
			if (db.isOpen()) {
				hasProblem = true;
				if (db.inTransaction()) {
					db.endTransaction();
					hasProblem = true;
				}
				db.close();
			}
		}
		return hasProblem;
	}

}

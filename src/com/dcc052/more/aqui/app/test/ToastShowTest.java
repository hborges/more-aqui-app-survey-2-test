package com.dcc052.more.aqui.app.test;

import android.test.ActivityInstrumentationTestCase2;
import android.view.Gravity;
import android.widget.Toast;

import com.dcc052.more.aqui.app.InsertActivity;
import com.jayway.android.robotium.solo.Solo;

public class ToastShowTest extends ActivityInstrumentationTestCase2<InsertActivity> {

	private static Solo solo;
	private static InsertActivity mActivity;
	
	private static boolean executed = false;
	
	private static boolean show = false;
	private static boolean layout = false;
	private static boolean duracao = false;
	private static boolean posicionamento = false;

	private static final String MESSAGE = "com.dcc052.more.aqui.app.test.InsertToastTest.testToast()";
	
	private static final String MESSAGE_LAYOUT_NAO_CUSTOMIZADO = "A mensagem não está sendo apresentada no layout customizado!";
	private static final String MESSAGE_NAO_LONGA = "A duração da mensagem não é longa!";
	private static final String MESSAGE_POSICIONAMENTO_INCORRETO = "A mensagem não está sendo apresentada no centro e na horizontal!";
	private static final String MESSAGE_NAO_APARECE = "A mensagem não está sendo apresentada!";
	
	public ToastShowTest() {
		super(InsertActivity.class);
	}
	
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.beforeClass();
	}
	
	public void testLayoutCustomizado() {
		assertTrue(MESSAGE_LAYOUT_NAO_CUSTOMIZADO, layout);
	}
	
	public void testDuracaoDaMensagem () {
		assertTrue(MESSAGE_NAO_LONGA, duracao);
	}
	
	public void testPosicionamento() {
		assertTrue(MESSAGE_POSICIONAMENTO_INCORRETO, posicionamento);
	}
	
	public void testToast () {
		assertTrue(MESSAGE_NAO_APARECE, show);
	}
	
	private void beforeClass() throws InterruptedException {
		if (!executed) {
			mActivity = super.getActivity();
			solo = new Solo(getInstrumentation(), mActivity);
			Thread t = new Thread(new Runnable() {
				public void run() {
					mActivity.makeAndShowToast(MESSAGE);
				}
			});
			mActivity.runOnUiThread(t);
			while(t.isAlive()){Thread.sleep(100);}
			show = solo.waitForText(MESSAGE);
			layout = mActivity.getDefaultToast().getView() != null;
			duracao = mActivity.getDefaultToast().getDuration() == Toast.LENGTH_LONG;
			posicionamento = mActivity.getDefaultToast().getGravity() == Gravity.CENTER_HORIZONTAL;
			executed = true;
		}
	}

}
